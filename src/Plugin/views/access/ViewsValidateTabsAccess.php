<?php
/**
 *
 * @ingroup views_access_plugins
 *
 * @ViewsAccess(
 *   id = "views_validate_tabs.access_handler",
 *   title = @Translation("Validated context tab access"),
  * )
 */

namespace Drupal\views_validate_tabs\Plugin\views\access;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Plugin\views\access\Permission;
use Symfony\Component\Routing\Route;

class ViewsValidateTabsAccess extends Permission {

  public function summaryTitle() {
    return $this->t('Views validate tabs access with permission: %permission', ["%permission" => parent::summaryTitle()]);
  }

  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['markup'] = [
      '#type' => 'container',
      '#markup' => $this->t('Set menu to tab and use a valid tab path (e.g.: node/%node/example-tab) in page settings and validate entity with bundles in contextual filter'),
    ];
  }

  /**
   * Add entity and bundle to route parameters options taking it from contextual filter validate options
   *
   * @param Route $route
   */
  public function alterRouteDefinition(Route $route) {
    $valid_parameters = [];
    foreach($this->displayHandler->getOption("arguments") as $argument) {
      if ($argument['specify_validation']) {
        $valid_parameters[$argument['entity_type']] = ['type' => "entity:{$argument['entity_type']}"];
        if (isset($argument['validate_options']['bundles'])){
          $valid_parameters[$argument['entity_type']]['bundle'] = [];
          foreach($argument['validate_options']['bundles'] as $bundle) {
            $valid_parameters[$argument['entity_type']]['bundle'][] = $bundle;
          }
        }
      }
    }
    if (isset($valid_parameters)) {
      $route->setOption('parameters', $valid_parameters);
    }
    // When not setting the requirement on permission the parameters are not validated
    $route = parent::alterRouteDefinition($route);
  }

}
