## INTRODUCTION

Create a view, such as nodes associated with a group or taxonomy term, and add validation on the bundle, as well as a local task.

On entities with a different bundle, the local task will be hidden when the view itself with be a 404 or 403 depending on the argument configuration. 


## REQUIREMENTS

views

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Add the tab as in https://www.webwash.net/custom-tab-user-profile-page-views-drupal-8/
- Configure the validator.
- Add the access plugin in access restriction configuration.

## MAINTAINERS

Current maintainers for Drupal 10:

- Aleix Quintana Alsius (aleix) - https://www.drupal.org/u/aleix

